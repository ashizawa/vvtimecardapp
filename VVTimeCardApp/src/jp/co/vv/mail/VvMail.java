/*
 * VvMail.java
 * 
 *  Created on H.Ashizawa 2014/07.04  新規作成
 */
package jp.co.vv.mail;

import android.content.Intent;

/**
 * メールクラス
 * 
 * @author H.Ashizawa
 */
public class VvMail {
	
	public static Intent send() {
		// Intentインスタンスを生成  
		Intent intent = new Intent();  
		  
		// アクションを指定
		intent.setAction(Intent.ACTION_SEND);  
		// データタイプを指定  
		intent.setType("message/rfc822");  
		// メーラー"Gmail"
		intent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
		// 宛先を指定  
		intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"ashizawa@vv-net.co.jp"});  
		// CCを指定  
		intent.putExtra(Intent.EXTRA_CC, new String[]{""});  
		// BCCを指定  
		intent.putExtra(Intent.EXTRA_BCC, new String[]{""});  
		// 件名を指定  
		intent.putExtra(Intent.EXTRA_SUBJECT, "");  
		// 本文を指定  
		intent.putExtra(Intent.EXTRA_TEXT, "本文の内容");  

		return intent;
	}
}
