package jp.co.vv.define;

/**
 * 定数
 * 
 * @author H.Ashizawa
 */
public class ConstDefine {
	
	// 各画面のリクエストコード
	/** 勤怠入力画面 */
	public static final int TIME_CARD_MAIN_ACTIVITY = 1;
	/** コメント入力画面 */
	public static final int COMMENT_ACTIVITY = 2;
	/** 設定メニュー画面 */
	public static final int SETTING_MENU_ACTIVITY = 3;
	/** アカウント設定画面 */
	public static final int SETTING_ACCOUNT_ACTIVITY = 4;
	/** 勤務時間設定画面 */
	public static final int SETTING_WORK_TIME_ACTIVITY = 5;
	
	// INTENTキー
	/** コメント入力 */
	public static final String INTENT_KEY_COMMENT = "INTENT_KEY_COMMENT";
	
	// SharedPreference
	/** リファレンス名(設定情報保存用) */
	public static final String PREF_NAME = "PREF_SETTING_DATA";
	/** プリファレンスキー（初回起動） */
	public static final String PREF_KEY_FIRST_BOOT = "firstBoot";
	/** プリファレンスキー（ユーザID） */
	public static final String PREF_KEY_USER_ID = "userId";
	/** プリファレンスキー(パスワード） */
	public static final String PREF_KEY_PASSWORD = "password";
	/** プリファレンスキー(始業時間） */
	public static final String PREF_KEY_START_TIME = "startTime";
	/** プリファレンスキー(就業時間） */
	public static final String PREF_KEY_END_TIME = "endTime";
	/** プリファレンスキー(休憩時間） */
	public static final String PREF_KEY_BREAK_TIME = "breakTime";
	/** プリファレンスキー(社員名） */
	public static final String PREF_KEY_WORKER_NM = "workerNm";
	
	// 処理判定 
	/** 出社 */
	public static final int ATTENDANCE = 1;
	/** 退社 */
	public static final int RESIGNATION = 2;
	
	// 日時セパレータ
	/** 日付セパレータ */
	public static final String DATE_SEPARATOR = "-";
	/** 時間セパレータ */
	public static final String TIME_SEPARATOR = ":";
	
	
}
