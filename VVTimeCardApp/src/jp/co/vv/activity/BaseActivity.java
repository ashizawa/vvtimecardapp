/*
 * BaseActivity.java
 * 
 * Created on H.Ashizawa 2014.07.02 新規作成
 */

package jp.co.vv.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * 共通Activity
 * 
 * @author H.Ashizawa
 */
public class BaseActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
	}
	
	/**
	 * キーイベントの取得
	 */
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
	    if (event.getAction()==KeyEvent.ACTION_DOWN) {
	        switch (event.getKeyCode()) {
	        case KeyEvent.KEYCODE_BACK:
	        	// 端末の戻るボタンを無効にする。(iOSと操作性を合わせる)
	        	Toast.makeText(this, "「戻る」ボタンの操作は無効です。", Toast.LENGTH_SHORT).show();
	            return true;
	        }
	    }
	    return super.dispatchKeyEvent(event);
	}

	/**
	 * タイトルバーの「戻る」ボタンタップ処理(直前の画面に戻る)
	 * @param v
	 */
	public void btnBack_Click(View v) {
		finish();
	}
	
	/**
	 * タイトルバーボタンを非表示にする(スペースは詰めない)
	 */
	protected void inVisibleBtnTitleBar(int buttonId) {
		ImageButton imgBtnTitleBar2 = (ImageButton)findViewById(buttonId);
		imgBtnTitleBar2.setVisibility(View.INVISIBLE);
	}
	
}
