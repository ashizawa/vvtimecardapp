/*
 * TimeCardMain.java
 * 
 * Created on H.Ashizawa 2014.07.02 新規作成
 */
package jp.co.vv.activity;

import java.util.HashMap;

import jp.co.vv.common.CommonUtil;
import jp.co.vv.common.VvDialog;
import jp.co.vv.define.ConstDefine;
import jp.co.vv.vvtimecardapp.R;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

/**
 * 勤怠入力画面
 * 
 * @author ashizawa
 */
public class TimeCardMainActivity extends BaseActivity {
	
	/** 社員名表示部 */
	private TextView txtWorkerName;
	/** 日付選択ダイアログ */
	private DatePickerDialog datePickerDialog;
	/** 時間選択ダイアログ */
	private TimePickerDialog timePickerDialog;
	/** 日付表示部 */
	private TextView txtDate;
	/** 現在時刻表示部 */
	private TextView txtNowTime;
	/** 出社時刻項目表示部 */
	@SuppressWarnings("unused")
	private TextView txtAttendanceTime;
	/** 出社時刻表示部 */
	private TextView txtAttendanceTimeValue;
	/** 退社時刻項目表示部 */
	@SuppressWarnings("unused")
	private TextView txtResignationTime;
	/** 退社時刻表示部 */
	private TextView txtResignationTimeValue;
	/** 勤務状況スピナー */
	private Spinner spinner_WorkStatus;
	/** 稼働状況スピナー */
	private Spinner spinner_OperatingStatus;
	/** 休憩時間 */
	TextView txtBreakTime;
	/** 社内時間 */
	TextView txtHouseWorkTime;
	/** コメント表示部 */
	private TextView txtComment;
	
	/**
	 * 画面初回表示時のみ実行される処理
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.time_card_main);
		
		// 多重起動確認
		if((getIntent().getFlags()&Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT)!=0) {
			finish();
			return;
		}
		
		// 初回起動確認
		if(CommonUtil.isFirstBoot(this)) {
			// ** 初回起動 ******
			// アカウント設定画面表示
        	Intent intent = new Intent(this, SettingAcconutActivity.class);
    		startActivityForResult(intent, ConstDefine.SETTING_ACCOUNT_ACTIVITY);
    		
    		// 始業、終業、休憩時間のデフォルト値登録
    		initWorkingTime();
    		
    		// 勤怠入力画面は一度破棄する
    		finish();
		}
		
		txtWorkerName = (TextView)findViewById(R.id.txtValueUserNm);
		txtDate = (TextView)findViewById(R.id.txtDate);
		txtNowTime = (TextView)findViewById(R.id.txtNowTime);
		txtAttendanceTime = (TextView)findViewById(R.id.txtAttendanceTime);
		txtAttendanceTimeValue = (TextView)findViewById(R.id.txtAttendanceTimeValue);
		txtResignationTime = (TextView)findViewById(R.id.txtResignationTime);
		txtResignationTimeValue = (TextView)findViewById(R.id.txtResignationTimeValue);
		spinner_WorkStatus = (Spinner)findViewById(R.id.spinner_WorkStatus);
		spinner_OperatingStatus = (Spinner)findViewById(R.id.spinner_OperatingStatus);
		txtBreakTime = (TextView)findViewById(R.id.txtBreakTime);
		txtHouseWorkTime = (TextView)findViewById(R.id.txtHouseWorkTime);
		txtComment = (TextView)findViewById(R.id.txtComment);
				
		// 各スピナーの初期化
		initSpinner();
	}
	
	/**
	 * 表示時（再表示含む）に必ず実行される処理
	 */
	@Override
	protected void onResume() {
		super.onResume();
		
		// 社員名、現在日付、現在時刻の表示
		txtWorkerName.setText(CommonUtil.getPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_WORKER_NM));
		txtDate.setText(CommonUtil.getDate());
		txtNowTime.setText(CommonUtil.getTime());
		
		// 休憩時間の表示
		String breakTime = CommonUtil.getPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_BREAK_TIME);
		if(breakTime != null) {
			txtBreakTime.setText(breakTime);
		}
	}

	/**
	 * 始業、終業、休憩時間のデフォルト値登録
	 */
	private void initWorkingTime() {
		String startTime = getText(R.string.default_start_time).toString();
		String endTime = getText(R.string.default_end_time).toString();
		String breakTime = getText(R.string.default_break_time).toString();
		
		CommonUtil.setPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_START_TIME, startTime);
		CommonUtil.setPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_END_TIME, endTime);
		CommonUtil.setPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_BREAK_TIME, breakTime);
	}
	
	/**
	 * 各スピナーの初期化
	 */
	private void initSpinner() {
		initSpinnerWorkStatus();		// 勤務状況
		initSpinner_OperatingStatus();	// 稼働状況
	}
	
	/**
	 * 勤務状況スピナーの初期化
	 */
	private void initSpinnerWorkStatus() {
		CommonUtil.setSpinner(this, R.array.WorkStatusList, spinner_WorkStatus);
	}
	
	/**
	 * 稼働状況スピナーの初期化
	 */
	private void initSpinner_OperatingStatus() {
		CommonUtil.setSpinner(this, R.array.OperatingStatusList, spinner_OperatingStatus);
	}
	
	/**
	 * コメント入力部のクリック処理
	 */
	public void UiComment_Click(View v) {
		// コメント入力画面を表示する。(現在設定されているコメントを受渡す)
		Intent intent = new Intent(this, CommentActivity.class);
		intent.putExtra(ConstDefine.INTENT_KEY_COMMENT, txtComment.getText().toString());
		startActivityForResult(intent, ConstDefine.COMMENT_ACTIVITY);
	}
	
	/**
	 * Intentの戻り値を取得
	 */
	@Override  
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {  
        super.onActivityResult(requestCode, resultCode, intent);
        
        // コメント入力画面より
        if (requestCode == ConstDefine.COMMENT_ACTIVITY){
        	// 入力を確定した
            if (resultCode == RESULT_OK){  
                Bundle extras = intent.getExtras();  
                if (extras != null){  
                	txtComment.setText(extras.getString(ConstDefine.INTENT_KEY_COMMENT));
                }else{  
                }
            // 入力をキャンセルした
            }else{
            	;
            }  
        }  
    }  


	/**
	 * アプリの終了（プロセスのキル）
	 * @param v
	 */
	public void btnAppEnd_Click(View v) {
		DialogInterface.OnClickListener positive = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	// アプリ終了
            	android.os.Process.killProcess(android.os.Process.myPid());
            }
        };
        DialogInterface.OnClickListener negative = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	;
            }
        };
        VvDialog.show(this, VvDialog.ICON_INFO, R.string.title_check, R.string.INFO_0001,
        				R.string.btnYes, R.string.btnNo, positive, negative, false);
	}
	
	/**
	 * 「休憩時間クリア」ボタンのクリック処理
	 * @param v
	 */
	public void btnBreakTimeClear_Click(View v) {
		txtBreakTime.setText("");
	}
	
	/**
	 * 「社内時間クリア」ボタンのクリック処理
	 * @param v
	 */
	public void btnHouseWorkTimeClear_Click(View v) {	
		txtHouseWorkTime.setText("");
	}

	/**
	 * 設定ボタンのクリック処理
	 */
	public void btnSetting_Click(View v) {
		// コメント入力画面を表示する。
		Intent intent = new Intent(this, SettingMainActivity.class);
		startActivity(intent);
	}
	
	/**
	 * 出社時間項目のクリック処理
	 * @param v
	 */
	public void UiAttendanceTime_Click(View v) {
		// 現在時刻を出社時間に設定
		txtAttendanceTimeValue.setText(CommonUtil.getTime().toString());
	}
	
	/**
	 * 出社時間のクリック処理
	 * @param v
	 */
	public void UiAttendanceTimeValue_Click(View v) {
		// 入力されている時間を取得
		String time = txtAttendanceTimeValue.getText().toString();
		if(time.length() <= 0) {
			// 入力されていない場合は現在時刻でTimePickerを表示
			timePickerDialog = CommonUtil.setTimePicker(this, AttendanceTimeSetListener, false);
		} else {
			// 入力されている場合は設定時刻でTimePickerを表示
			timePickerDialog = CommonUtil.setTimePicker(this, AttendanceTimeSetListener, time);
		}
		timePickerDialog.show();
	}
	//出社時間設定時のリスナ登録
	TimePickerDialog.OnTimeSetListener AttendanceTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    	txtAttendanceTimeValue.setText(CommonUtil.timeFormat(hourOfDay, minute));
	    }
	};
	
	/**
	 * 退社時間項目のクリック処理
	 * @param v
	 */
	public void UiResignationTime_Click(View v) {
		// 現在時刻を退社時間に設定
		txtResignationTimeValue.setText(CommonUtil.getTime().toString());
	}
	
	/**
	 * 退社時間のクリック処理
	 * @param v
	 */
	public void UiResignationTimeValue_Click(View v) {
		// 入力されている時間を取得
		String time = txtResignationTimeValue.getText().toString();
		if(time.length() <= 0) {
			// 入力されていない場合は現在時刻でTimePickerを表示
			timePickerDialog = CommonUtil.setTimePicker(this, ResignationTimeSetListener, false);
		} else {
			// 入力されている場合は設定時刻でTimePickerを表示
			timePickerDialog = CommonUtil.setTimePicker(this, ResignationTimeSetListener, time);
		}
		timePickerDialog.show();
	}
	//退社時間設定時のリスナ登録
	TimePickerDialog.OnTimeSetListener ResignationTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    	txtResignationTimeValue.setText(CommonUtil.timeFormat(hourOfDay, minute));
	    }
	};
	
	/**
	 * 日付表示エリアのクリック処理
	 */
	public void UiDate_Click(View v) {
		// 入力されている時間を取得
		String date = txtDate.getText().toString();
		if(date.length() <= 0) {
			// 入力されていない場合は現在時刻でTimePickerを表示
			datePickerDialog = CommonUtil.setDatePicker(this, DateSetListener, false);
		} else {
			// 入力されている場合は設定時刻で表示TimePickerを表示
			datePickerDialog = CommonUtil.setDatePicker(this, DateSetListener, date);
		}
        datePickerDialog.show();
	}
	//日付設定時のリスナ登録
    DatePickerDialog.OnDateSetListener DateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(android.widget.DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        	txtDate.setText(CommonUtil.dateFormat(year, monthOfYear, dayOfMonth));
        }
    };
        
	/**
	 * 休憩時間表示エリアのクリック処理
	 */
	public void UiBreakTime_Click(View v){
		// 入力されている時間を取得
		String time = txtBreakTime.getText().toString();
		
		if(time.length() <= 0) {
			// 入力されていない場合は現在時刻でTimePickerを表示
			timePickerDialog = CommonUtil.setTimePicker(this, BreakTimeSetListener, true);
		} else {
			// 入力されている場合は設定時刻でTimePickerを表示
			timePickerDialog = CommonUtil.setTimePicker(this, BreakTimeSetListener, time);
		}
		timePickerDialog.show();
	}
	//時刻設定時のリスナ登録
	TimePickerDialog.OnTimeSetListener BreakTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    	txtBreakTime.setText(CommonUtil.timeFormat(hourOfDay, minute));
	    }
	};
    
	/**
	 * 社内時間表示エリアのクリック処理
	 */
	public void UiHouseWorkTime_Click(View v){
		// 入力されている時間を取得
		String houseWorkTime = txtHouseWorkTime.getText().toString();
		
		if(houseWorkTime.length() <= 0) {
			// 入力されていない場合は現在時刻でTimePickerを表示
			timePickerDialog = CommonUtil.setTimePicker(this, HouseTimeSetListener, true);
		} else {
			// 入力されている場合は設定時刻でTimePickerを表示
			timePickerDialog = CommonUtil.setTimePicker(this, HouseTimeSetListener, houseWorkTime);
		}
		timePickerDialog.show();
	}
	//時刻設定時のリスナ登録
	TimePickerDialog.OnTimeSetListener HouseTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    	txtHouseWorkTime.setText(CommonUtil.timeFormat(hourOfDay, minute));
	    }
	};
	
    /**
     * 「出社」ボタンタップ処理
     * @param v
     */
    public void btnAttendance_Click(View v) {
    	sendTimeCardData(ConstDefine.ATTENDANCE);
    }
    
    /**
     * 「退社」ボタンタップ処理
     * @param v
     */
    public void btnResignation_Click(View v) {
    	sendTimeCardData(ConstDefine.RESIGNATION);
    }
    
    /**
     * 勤怠データ送信(登録)処理
     * 
     * @param mode ConstDefine.ATTENDANCE(=1):出勤, ConstDefine.RESIGNATION(=2):退勤
     */
    private void sendTimeCardData(int mode) {
    	
    	// ** 各種入力データの取得　************
    	// 日付
    	String date = txtDate.getText().toString();
    	
    	// 分割日付
		@SuppressWarnings("unchecked")
		HashMap<String, Integer>split_date = CommonUtil.splitDate(date);
		int year = split_date.get("YEAR");
		int month = split_date.get("MONTH");
		int day = split_date.get("DAY");
		
		// 出社時間
		String startTime = txtAttendanceTimeValue.getText().toString();
		startTime = CommonUtil.timeFormat(startTime);
		
		// 退社時間
		String endTime = txtResignationTimeValue.getText().toString();
		endTime = CommonUtil.timeFormat(endTime);
		
    	// 勤務状況
    	String workStatus = (String)spinner_WorkStatus.getSelectedItem();
    	// 稼働状況
    	String operatingStatus = (String)spinner_OperatingStatus.getSelectedItem();
    	// コメント
    	String comment = txtComment.getText().toString();
    	// 社内時間
    	String houseWorkTime = txtHouseWorkTime.getText().toString();
    	if(!"".equals(houseWorkTime)) {
    		houseWorkTime = CommonUtil.timeFormat(houseWorkTime);
    	}
    	// 休憩時間
    	String breakTime = txtBreakTime.getText().toString();
    	if(!"".equals(breakTime)) {
    		breakTime = CommonUtil.timeFormat(breakTime);
    	}
    	
    	Log.d("DEBUG-VV","date :" + date);
    	Log.d("DEBUG-VV","year :" + year);
    	Log.d("DEBUG-VV","month :" + month);
    	Log.d("DEBUG-VV","day :" + day);
    	Log.d("DEBUG-VV","startTime :" + startTime);
    	Log.d("DEBUG-VV","endTime :" + endTime);
    	Log.d("DEBUG-VV","houseWorkTime : " + houseWorkTime);
    	Log.d("DEBUG-VV","workStatus : " + workStatus);
    	Log.d("DEBUG-VV","operatingStatus : " + operatingStatus);
    	Log.d("DEBUG-VV","comment : " + comment);
    	Log.d("DEBUG-VV","breakTime : " + breakTime);
    	
    	
    	if(mode == ConstDefine.ATTENDANCE) {
    		// 出社
    		// 退社時間自動設定（勤務時間設定より取得）
    		// FIXME : 送信処理いれる
    		
    	} else if(mode == ConstDefine.RESIGNATION) {
    		// 退社(出社時間は日付を元にサーバから取得)
    		// FIXME : 送信処理いれる
    	}
    	
    	// FIXME : 以下、それぞれのダイアログ
    	// 出勤　送信成功
		VvDialog.show(this, VvDialog.ICON_INFO, R.string.title_startTime, R.string.INFO_0003, R.string.btnYes, false);
		// 退勤　送信成功 
		VvDialog.show(this, VvDialog.ICON_INFO, R.string.title_EndTime, R.string.INFO_0003, R.string.btnYes, false);
		// 送信失敗
		VvDialog.show(this, VvDialog.ICON_ERROR, R.string.title_Error, R.string.ERR_0001, R.string.btnYes, false);
		// 通信エラー(圏外)
		VvDialog.show(this, VvDialog.ICON_ERROR, R.string.title_Error, R.string.ERR_0005, R.string.btnYes, false);
    }
}
