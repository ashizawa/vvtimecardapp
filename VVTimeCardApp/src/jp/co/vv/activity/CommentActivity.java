/*
 * CommentActivity.java
 * 
 * Created on H.Ashizawa 2014.07.02 新規作成
 */

package jp.co.vv.activity;

import java.util.HashMap;

import jp.co.vv.common.DeviceInfo;
import jp.co.vv.common.VvDialog;
import jp.co.vv.define.ConstDefine;
import jp.co.vv.vvtimecardapp.R;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * コメント入力画面
 * 
 * @author H.ashizawa
 */
public class CommentActivity extends BaseActivity{
	
	/** コメント入力部 */
	private EditText editText;
		
	/**
	 * 画面初回表示時に実行される処理
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// タイトルバーを非表示にする
		requestWindowFeature(Window.FEATURE_NO_TITLE);
 
		// レイアウト設定
		setContentView(R.layout.comment);
		
		// 画面サイズの取得
		@SuppressWarnings("unchecked")
		HashMap<String, Float> displaySize = DeviceInfo.getDisplaySize(this);
		float width = displaySize.get("width");
		float height = displaySize.get("height");
		
		// 画面調整サイズ取得
		int ajustWidth = Integer.parseInt(getText(R.string.comment_disp_ajust_width).toString());
		int ajustHeight = Integer.parseInt(getText(R.string.comment_disp_ajust_height).toString());
		
		// 画面サイズの設定
		getWindow().setLayout((int)width-ajustWidth, (int)height-ajustHeight);
	
		// 呼出元での入力値を取得する。
		Intent intent = getIntent();
		String comment = intent.getStringExtra(ConstDefine.INTENT_KEY_COMMENT);
		
		// 入力済みのコメントを表示する。
		editText = (EditText)findViewById(R.id.editComment);
		editText.setText(comment);
		
	}
	
	/** 
	 * 「クリア」ボタン
	 * @param v
	 */
	public void btnClear_Click(View v) {
		DialogInterface.OnClickListener positive = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	// 入力内容クリア
            	editText.setText("");
            }
        };
        DialogInterface.OnClickListener negative = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	// 何もしない
            	;
            }
        };
        VvDialog.show(this, VvDialog.ICON_INFO, R.string.title_check, R.string.INFO_0002,
        				R.string.btnYes, R.string.btnNo, positive, negative, false);
	}
	
	/**
	 * 「入力」ボタンタップ処理
	 * @param v
	 */
	public void btnInput_Click(View v) {
		// 入力を確定して呼出元画面に戻る(呼出元には入力したコメントを渡す)
		EditText editComment = (EditText)findViewById(R.id.editComment);  
        Intent intent = new Intent();  
        intent.putExtra(ConstDefine.INTENT_KEY_COMMENT, editComment.getText().toString());  
        setResult(RESULT_OK,intent);  
        finish();  
	}
	
	/**
	 * 「キャンセル」ボタンタップ処理
	 * @param v
	 */
	public void btnCancel_Click(View v) {
		// 入力を確定せずに前画面に戻る
		setResult(RESULT_CANCELED);  
        finish();  
	}

	/**
	 * タイトルバーの「戻る」ボタンタップ処理(直前の画面に戻る)
	 * @param v
	 */
	@Override
	public void btnBack_Click(View v) {
		super.btnBack_Click(v);
	}
}
