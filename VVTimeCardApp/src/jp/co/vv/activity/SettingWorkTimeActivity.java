/*
 * SettingWorkTimeActivity.java
 * 
 * Created on H.Ashizawa 2014.07.03 新規作成
 */
package jp.co.vv.activity;

import jp.co.vv.common.CommonUtil;
import jp.co.vv.common.VvDialog;
import jp.co.vv.define.ConstDefine;
import jp.co.vv.vvtimecardapp.R;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

/**
 * 勤務時間設定画面
 * 
 * @author H.Ashizawa
 */
public class SettingWorkTimeActivity extends BaseActivity{
	
	/** 時間選択ダイアログ */
	private TimePickerDialog timePickerDialog;
	/** 始業時間入力部 */
	private TextView txtStartTime;
	/** 終業時間入力部 */
	private TextView txtEndTime;
	/** 休憩時間入力部 */
	private TextView txtBreakTime;
	
	/**
	 * 画面初回表示時に実行される処理
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_work_time);
		
		// タイトルバー右側のボタンを非表示にする(スペースは詰めない)
		inVisibleBtnTitleBar(R.id.imgBtnTitleBar2);
		
		txtStartTime = (TextView)findViewById(R.id.txtStartTimeValue);
		txtEndTime = (TextView)findViewById(R.id.txtEndTimeValue);
		txtBreakTime = (TextView)findViewById(R.id.txtBreakTimeDefaultValue);
		
		// すでに入力されたデータが存在する場合は初期表示する
		String startTime = CommonUtil.getPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_START_TIME);
		String endTime = CommonUtil.getPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_END_TIME);
		String breakTime = CommonUtil.getPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_BREAK_TIME);
		if(startTime == null) {
	    	txtStartTime.setText(startTime);
		}
		if(endTime == null) {
	    	txtEndTime.setText(endTime);
		}
		if(breakTime == null) {
	    	txtBreakTime.setText(breakTime);
		}
	}
	
	/**
	 * 始業時間入力部のタップ処理
	 * 
	 * @param v
	 */
	public void UiStartTime_Click(View v) {
		// 入力されている時間を取得
		String time = txtStartTime.getText().toString();
		timePickerDialog = setTimePicker(StartTimeSetListener, time);
		timePickerDialog.show();
	}
	//時刻設定時のリスナ登録
	TimePickerDialog.OnTimeSetListener StartTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    	txtStartTime.setText(CommonUtil.timeFormat(hourOfDay, minute));
	    }
	};
		
	/**
	 * 就業時間入力部のタップ処理
	 * 
	 * @param v
	 */
	public void UiEndTime_Click(View v) {
		String time = txtEndTime.getText().toString();
		timePickerDialog = setTimePicker(EndTimeSetListener, time);
		timePickerDialog.show();
	}
	//時刻設定時のリスナ登録
	TimePickerDialog.OnTimeSetListener EndTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    	txtEndTime.setText(CommonUtil.timeFormat(hourOfDay, minute));
	    }
	};
	
	/**
	 * 休憩時間入力部のタップ処理
	 * 
	 * @param v
	 */
	public void UiBreakTime_Click(View v) {
		String time = txtBreakTime.getText().toString();
		timePickerDialog = setTimePicker(BreakTimeSetListener, time);
		timePickerDialog.show();
	}
	//時刻設定時のリスナ登録
	TimePickerDialog.OnTimeSetListener BreakTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
	    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    	txtBreakTime.setText(CommonUtil.timeFormat(hourOfDay, minute));
	    }
	};
	
	/**
	 * TimePickerの設定
	 * 
	 * @param event　イベントリスナー
	 * @param time　設定時間
	 * @return　TimePickerオブジェクト
	 */
	private TimePickerDialog setTimePicker(TimePickerDialog.OnTimeSetListener event, String time) {
		if(time.length() <= 0) {
			// 入力されていない場合は現在時刻でTimePickerを表示
			timePickerDialog = CommonUtil.setTimePicker(this, event, true);
		} else {
			// 入力されている場合は設定時刻で表示TimePickerを表示
			timePickerDialog = CommonUtil.setTimePicker(this, event, time);
		}
		return timePickerDialog;
	}
	
	/** 
	 * 「設定」ボタンタップ処理
	 * @param v
	 */
	public void btnSet_Click(View v) {
		DialogInterface.OnClickListener positive = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	// 設定
        		String startTime = txtStartTime.getText().toString();
        		String endTime = txtEndTime.getText().toString();
        		String breakTime = txtBreakTime.getText().toString();
        		CommonUtil.setPreferenceForString(SettingWorkTimeActivity.this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_START_TIME, startTime);
        		CommonUtil.setPreferenceForString(SettingWorkTimeActivity.this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_END_TIME, endTime);
        		CommonUtil.setPreferenceForString(SettingWorkTimeActivity.this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_BREAK_TIME, breakTime);
        		finish();
            }
        };
        DialogInterface.OnClickListener negative = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	// 何もしない
            	;
            }
        };
		VvDialog.show(
				this, VvDialog.ICON_INFO, R.string.title_check, R.string.INFO_0005,
				R.string.btnYes, R.string.btnNo, positive, negative, false);
	}
	
	/** 
	 * 「キャンセル」ボタンタップ処理
	 * @param v
	 */
	public void btnCancel_Click(View v) {
		finish();
	}
}
