/*
 * SettingMainActivity.java
 * 
 * Created on H.Ashizawa 2014.07.02 新規作成
 */

package jp.co.vv.activity;

import jp.co.vv.define.ConstDefine;
import jp.co.vv.mail.VvMail;
import jp.co.vv.vvtimecardapp.R;
import android.content.Intent;
import android.graphics.Color;
import android.net.MailTo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 設定メニュー画面
 * 
 * @author H.Ashizawa
 */
public class SettingMainActivity extends BaseActivity {
	
	/** 設定メニュー */
	ListView settingMenuList;
	
	/**
	 * 画面初回表示時に実行される処理
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_main);
		
		// タイトルバー右側のボタンを非表示にする(スペースは詰めない)
		inVisibleBtnTitleBar(R.id.imgBtnTitleBar2);
		
		// メニュー項目取得(string.xmlより)
		String[] timeCardSetthingMenu = getResources().getStringArray(R.array.timeCardSetthingMenu);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, timeCardSetthingMenu ) {
			/**メニュー項目設定 */
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				TextView view = (TextView)super.getView(position, convertView, parent);
				view.setTextSize(30);				// テキストサイズ(単位:dp)
				view.setTextColor(Color.BLACK);		// テキストカラー
				return view;
			}
		};
		
		settingMenuList = (ListView)findViewById(R.id.settingMenuList);
		settingMenuList.setAdapter(adapter);
		
		settingMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /** リスト項目がクリックされた時の処理 */
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				moveDiaplay(parent, position);
            }
        });
         
		settingMenuList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			/** リスト項目が選択された時の処理 */
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            	moveDiaplay(parent, position);
            }
            /** リスト項目がなにも選択されていない時の処理 */
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
         
		settingMenuList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            /** リスト項目が長押しされた時の処理 */
			@Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				moveDiaplay(parent, position);
                return false;
            }
        });
	}
	
	/**
	 * 渡された設定名を元に対象の画面に遷移する。
	 * 
	 * @param item 選択された設定名
	 */
	private void moveDiaplay(AdapterView<?> parent, int position) {
		ListView listView = (ListView) parent;
        String item = (String) listView.getItemAtPosition(position);
        
        // アカウント設定画面へ
        if(item.equals(getApplicationContext().getText(R.string.setthig_account_menu))) {
        	Intent intent = new Intent(this, SettingAcconutActivity.class);
    		startActivityForResult(intent, ConstDefine.SETTING_ACCOUNT_ACTIVITY);
    	
    	// 	勤務時間設定画面へ
        } else if(item.equals(getApplicationContext().getText(R.string.setthig_work_time_menu))) {
        	Intent intent = new Intent(this, SettingWorkTimeActivity.class);
        	startActivityForResult(intent, ConstDefine.SETTING_WORK_TIME_ACTIVITY);

        // メール送信
        } else if(item.equals(getApplicationContext().getText(R.string.setthig_send_mail_menu))) {
        	Intent intent = VvMail.send();
        	startActivity(intent);
        }
	}
}
