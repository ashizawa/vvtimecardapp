/*
 * SettingAcconutActivity.java
 * 
 * Created on H.Ashizawa 2014.07.03 新規作成
 */
package jp.co.vv.activity;

import jp.co.vv.cipher.VvCipher;
import jp.co.vv.common.CommonUtil;
import jp.co.vv.common.VvDialog;
import jp.co.vv.define.ConstDefine;
import jp.co.vv.vvtimecardapp.R;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

/**
 * アカウント設定画面
 * 
 * @author H.Ashizawa
 */
public class SettingAcconutActivity extends BaseActivity
{
	
	/** ユーザID入力部 */
	private EditText editUserId;
	/** パスワード入力部 */
	private EditText editPassword;
	/** 社員名入力部 */
	private EditText editWorkerNmValue;
	
	/**
	 * 画面初回表示時に実行される処理
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_account);
		
		editUserId = (EditText)findViewById(R.id.editUserIdValue);
		editPassword = (EditText)findViewById(R.id.editPasswordValue);
		editWorkerNmValue = (EditText)findViewById(R.id.editWorkerNmValue);
		
		// 初回起動確認
		if(CommonUtil.isFirstBoot(this)) {
			// ** 初回設定 ******
			// タイトルバー左のボタンを非表示にする(スペースは詰めない)
			inVisibleBtnTitleBar(R.id.imgBtnTitleBar1);
		}
		// タイトルバー右側のボタンを非表示にする(スペースは詰めない)
		inVisibleBtnTitleBar(R.id.imgBtnTitleBar2);
		
		// すでに入力されたデータが存在する場合は初期表示する
		String userId = CommonUtil.getPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_USER_ID);
		String password = CommonUtil.getPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_PASSWORD);
		String workerNm = CommonUtil.getPreferenceForString(this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_WORKER_NM);
		if(userId == null || password == null) {
			// 入力済みのデータは存在しないため、何もしない
			;
		} else {
	    	// 入力済みのデータが存在するため、復号化して表示
	    	userId = VvCipher.decript(userId);
	    	password = VvCipher.decript(password);
	    	if(userId == null || password == null) {
	    		// 失敗 FIXME
	    		VvDialog.show(SettingAcconutActivity.this, VvDialog.ICON_ERROR, R.string.title_Error,
	    				R.string.ERR_0003, R.string.btnYes, false);
	    		finish();
	    	}
	    	editUserId.setText(userId);
	    	editPassword.setText(password);
	    	editWorkerNmValue.setText(workerNm);
		}
	}
		
	/** 
	 * 「接続テスト」ボタンタップ処理
	 * @param v
	 */
	public void btnConnectTest_Click(View v) {
		
		// アカウント情報の取得
		String userId = editUserId.getText().toString();
		String password = editPassword.getText().toString();
		
		// 必須項目の入力チェック
		if(!CommonUtil.isRequiredDataCheck(userId) || !CommonUtil.isRequiredDataCheck(password)) {
			VvDialog.show(this, VvDialog.ICON_ERROR, R.string.title_Error, R.string.ERR_0004, R.string.btnYes, false);
			return;
		}
		
		// FIXME : ここに送信処理を入れる
		
		// FIXME : 以下、それぞれのダイアログ
    	// 認証成功
		VvDialog.show(this, VvDialog.ICON_INFO, R.string.title_Auth_OK, R.string.INFO_0004, R.string.btnYes, false);
		// 認証失敗 
		VvDialog.show(this, VvDialog.ICON_ERROR, R.string.title_Auth_NG, R.string.ERR_0002, R.string.btnYes, false);
		// 圏外
		VvDialog.show(this, VvDialog.ICON_ERROR, R.string.title_Error, R.string.ERR_0001, R.string.btnYes, false);
	}
	
	/** 
	 * 「設定」ボタンタップ処理
	 * @param v
	 */
	public void btnSet_Click(View v) {
		
		// 必須項目の入力チェック
		String userId = editUserId.getText().toString();
    	String password = editPassword.getText().toString();
		if(!CommonUtil.isRequiredDataCheck(userId) || !CommonUtil.isRequiredDataCheck(password)) {
			VvDialog.show(this, VvDialog.ICON_ERROR, R.string.title_Error, R.string.ERR_0004, R.string.btnYes, false);
			return;
		}
		
		DialogInterface.OnClickListener positive = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	// 入力値の取得
            	String userId = editUserId.getText().toString();
            	String password = editPassword.getText().toString();
            	String workerNm = editWorkerNmValue.getText().toString();
            	
            	// 暗号化
            	userId = VvCipher.encript(userId);
            	password = VvCipher.encript(password);
            	if(userId == null || password == null) {
            		// 失敗 FIXME
            		dialog.dismiss();
            		VvDialog.show(SettingAcconutActivity.this, VvDialog.ICON_ERROR, R.string.title_Error,
            				R.string.ERR_0003, R.string.btnYes, false);
            	}
            	else {
            		// 設定
            		CommonUtil.setPreferenceForString(SettingAcconutActivity.this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_USER_ID, userId);
            		CommonUtil.setPreferenceForString(SettingAcconutActivity.this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_PASSWORD, password);
            		CommonUtil.setPreferenceForString(SettingAcconutActivity.this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_WORKER_NM, workerNm);
            		
            		// 初回起動確認
            		if(CommonUtil.isFirstBoot(SettingAcconutActivity.this)) {
            			// ** 初回起動(初回起動フラグを設定して勤怠入力画面を表示) ******
            			CommonUtil.setPreferenceForString(SettingAcconutActivity.this, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_FIRST_BOOT, "Compleated");
                    	Intent intent = new Intent(SettingAcconutActivity.this, TimeCardMainActivity.class);
                		startActivityForResult(intent, ConstDefine.TIME_CARD_MAIN_ACTIVITY);
            			finish();
            		} else {
            			// ** 2回目以降の起動(本画面を閉じる＝勤怠入力画面を表示) ******
            			finish();
            		}
            	}
            }
        };
        DialogInterface.OnClickListener negative = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	// 何もしない
            	;
            }
        };
		VvDialog.show(
				this, VvDialog.ICON_INFO, R.string.title_check, R.string.INFO_0005,
				R.string.btnYes, R.string.btnNo, positive, negative, false);
	}
	
	/** 
	 * 「キャンセル」ボタンタップ処理
	 * @param v
	 */
	public void btnCancel_Click(View v) {
		finish();
	}
}
