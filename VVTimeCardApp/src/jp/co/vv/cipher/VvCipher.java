/*
 * VvCipher.java
 * 
 * Create on H.Ashizawa 2014/07/03 新規作成
 */
package jp.co.vv.cipher;

import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

/** 
 * 暗号化/復号化クラス
 * 
 * @author H.Ashizawa
 */
public class VvCipher {
	
	private static final String KEY = "a1b2c3d4";
	private static final String ENCODING = "UTF-8";
	private static final String TYPE = "AES";
	
	private static SecretKeySpec makeKey() {
		
		SecretKeySpec key;
		 
		byte[] bytes = new byte[ 128 / 8 ];
		byte[] keys = null;
		try {
		     keys = KEY.getBytes(ENCODING);
		} catch (UnsupportedEncodingException e) {
		     e.printStackTrace();
		     return null;
		}
		for( int i = 0; i < KEY.length(); i++ )
		{
		     if( i >= bytes.length )
		          break;
		     bytes[ i ] = keys[ i ];
		}
		key = new SecretKeySpec(bytes, TYPE);
		return key;
	}
	
	public static String encript(String data) {
		SecretKeySpec key = makeKey();
		byte[] de = null;
		try {
		     Cipher cipher = Cipher.getInstance(TYPE);
		     cipher.init(Cipher.ENCRYPT_MODE, key);
		     de = cipher.doFinal(data.getBytes(ENCODING));
		} catch (Exception e) {
		     e.printStackTrace();
		     return null;
		}		
		return Base64.encodeToString(de, Base64.DEFAULT);
	}
	
	public static String decript(String data) {
		SecretKeySpec key = makeKey();
		byte[] en = null;
		try {
		     Cipher cipher = Cipher.getInstance( "AES" );
		     cipher.init(Cipher.DECRYPT_MODE, key);
		     en = cipher.doFinal(Base64.decode(data.getBytes(ENCODING), Base64.DEFAULT));
		} catch (Exception e) {
		     e.printStackTrace();
		     return null;
		}
		return new String(en);
	}
}
