/*
 * CommonUtil.java 
 */
package jp.co.vv.common;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import jp.co.vv.define.ConstDefine;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

/**
 * 共通部品
 */
public class CommonUtil {
	
	/**
	 * 初回起動確認
	 * 
	 * @param context コンテキスト
	 * @return true:初回起動<br>false:2回目以降の起動
	 */
	public static boolean isFirstBoot(Context context) {
		String firstBoot = CommonUtil.getPreferenceForString(context, ConstDefine.PREF_NAME, ConstDefine.PREF_KEY_FIRST_BOOT);
		if(firstBoot == null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 必須データチェック
	 * 
	 * @param data チェック対象のデータ
	 * @return　false:未入力あり<br>true:入力済み
	 */
	public static boolean isRequiredDataCheck(String data) {
		boolean result;
		// チェック
		if(data == null){
			result = false;
		}
		if(data.length() <= 0) {
			result = false;
		} else {
			result = true;
		}
		return result;
	}
	
	/**
	 * 指定したユーザプリファレンスの値を設定する
	 * 
	 * @param context コンテキスト
	 * @param prefName　プリファレンス名
	 * @param key　キー
	 * @param value 値
	 */
	public static void setPreferenceForString(Context context, String prefName, String key, String value) {
    	SharedPreferences prefs = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
    	SharedPreferences.Editor editor = prefs.edit();
    	editor.putString(key, value);
    	editor.apply();
	}
	
	/**
	 * 指定したユーザプリファレンスの値を取得する
	 * 
	 * @param context コンテキスト
	 * @param prefName　プリファレンス名
	 * @param key　キー
	 * 
	 * @return　値（存在しない場合は"null"を返す）
	 */
	public static String getPreferenceForString(Context context, String prefName, String key) {
		SharedPreferences prefs = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
		return prefs.getString(key, null);
	}

	/**
	 * 指定したスピナーへリストデータを追加（ID指定）
	 * 
	 * @param dataListId　リソースID
	 * @param spinerId　コントロールID
	 */
	public static void setSpinner(Context context, int dataListId, Spinner spinerId) {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        String[] data = context.getResources().getStringArray(dataListId);
        for (String dataList : data){
        	adapter.add(dataList);
        }
        spinerId.setAdapter(adapter);
	}
	
	/**
	 * 指定したスピナーへリストデータを追加（ArrayList[String型]指定）
	 * 
	 * @param dataList
	 * @param spinerId
	 */
	public static void setSpinner(Context context, ArrayList<String> dataList, Spinner spinerId) {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        for (String data : dataList){
        	adapter.add(data);
        }
        spinerId.setAdapter(adapter);
	}
	
	/**
	 * 現在日付を取得する。
	 * 
	 * @return　yyyy-mm-dd形式
	 */
	public static String getDate() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR); // 年
        int month = calendar.get(Calendar.MONTH); // 月
        int day = calendar.get(Calendar.DAY_OF_MONTH); // 日
        return dateFormat(year, month, day);
	}

	/**
	 * 引数で渡された日付データを所定のフォーマットに変換
	 * @param yyyy　年
	 * @param mm　月
	 * @param dd　日
	 * @return 　yyyy-mm-dd形式
	 */
	public static String dateFormat(int yyyy, int mm, int dd) {
		StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(yyyy));
        sb.append(String.valueOf(ConstDefine.DATE_SEPARATOR));
        if(mm < 10) {
        	sb.append("0");
        }
        sb.append(String.valueOf(mm + 1));
        sb.append(String.valueOf(ConstDefine.DATE_SEPARATOR));
        if(dd < 10) {
        	sb.append("0");
        }
        sb.append(String.valueOf(dd));
        return sb.toString();
	}
	
	/**
	 * 日付データを分割
	 * 
	 * @param date 分割する日付
	 * @return　分割後の日付（HashMap型) "key = YEAR/MONTH/DAY"
	 */
	@SuppressWarnings("rawtypes")
	public static HashMap splitDate(String date) {
		String[] split_date = date.split(ConstDefine.DATE_SEPARATOR);
		int yyyy = Integer.parseInt(split_date[0]);
		int mm = Integer.parseInt(split_date[1]);
		int dd = Integer.parseInt(split_date[2]);
		
		HashMap<String, Integer>date_map = new HashMap<String, Integer>();
		date_map.put("YEAR", yyyy);
		date_map.put("MONTH", mm);
		date_map.put("DAY", dd);
		
		return date_map;
	}
	
	/**
	 * DatePickerに現在の日付を設定
	 * 
	 * @param context コンテキスト
	 * @param eventListener 選択時に実行されるイベントリスナー
	 * @param init true:初期表示<br>false:現在日付表示
	 * @return DatePickerオブジェクト
	 */
	public static DatePickerDialog setDatePicker(Context context, DatePickerDialog.OnDateSetListener eventListener, boolean init) {
		// 現在時刻の取得
		String nowDate = CommonUtil.getDate();
		
		// 分割
		@SuppressWarnings("unchecked")
		HashMap<String, Integer>split_date = CommonUtil.splitDate(nowDate);
		int year = split_date.get("YEAR");
		int month = split_date.get("MONTH");
		int day = split_date.get("DAY");
		
		DatePickerDialog dp;
		if(init) {
			dp = new DatePickerDialog(context, android.R.style.Theme_Dialog, eventListener, year, 1, 1);
		} else {
			dp = new DatePickerDialog(context, android.R.style.Theme_Dialog, eventListener, year, (month-1), day);
		}
        return dp;
	}
	
	/**
	 * DatePickerに任意の日付を設定
	 * 
	 * @param context コンテキスト
	 * @param eventListener 選択時に実行されるイベントリスナー
	 * @param date 設定日付
	 * @return DatePickerオブジェクト
	 */
	public static DatePickerDialog setDatePicker(Context context, DatePickerDialog.OnDateSetListener eventListener, String date) {
		// 分割
		@SuppressWarnings("unchecked")
		HashMap<String, Integer>split_date = CommonUtil.splitDate(date);
		int year = split_date.get("YEAR");
		int month = split_date.get("MONTH");
		int day = split_date.get("DAY");
		
		return new DatePickerDialog(context, android.R.style.Theme_Dialog, eventListener, year, (month-1), day);
	}
	
	/**
	 * 現在時刻を取得する。
	 * 
	 * @return　hh:mm形式
	 */
	public static String getTime() {
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		return timeFormat(hour, minute);
	}
	
	/**
	 * 引数で渡された時間データを所定のフォーマットに変換
	 * @param hh　時
	 * @param mm　分
	 * 
	 * @return 　hh:mm形式
	 */
	public static String timeFormat(int hh, int mm) {
		
        StringBuilder sb = new StringBuilder();
		String hour = String.valueOf(hh);
		if(hour.length() == 1) {
			sb.append("0").append(hour);
		} else {
			sb.append(hour);
		}
		
		sb.append(String.valueOf(":"));
		
		String minute = String.valueOf(mm);
		if(minute.length() == 1) {
			sb.append("0").append(minute);
		} else {
			sb.append(minute);
		}

        return sb.toString();
	}
	
	/**
	 * 引数で渡された時間データを所定のフォーマットに変換
	 * 
	 * @param time hh:mm形式
	 * @return 　hhmm形式 (※不正な形式の値の場合は"null"を返す)
	 */
	public static String timeFormat(String time) {
		// ":"で分割
		String[] splitTimeData = time.split(":");
		if(splitTimeData.length != 2) {
			// 不正な値
			return null;
		}
		
		// ":"抜きで結合
        StringBuilder sb = new StringBuilder();
        sb.append(splitTimeData[0]).append(splitTimeData[1]);

        return sb.toString();
	}
	
	/**
	 * 時間データを分割
	 * 
	 * @param time 分割する時間
	 * @return　分割後の時間（HashMap型) "key = HOUR/MINUTE"
	 */
	@SuppressWarnings("rawtypes")
	public static HashMap splitTime(String time) {
		String[] split_time = time.split(ConstDefine.TIME_SEPARATOR);
		int hh = Integer.parseInt(split_time[0]);
		int mm = Integer.parseInt(split_time[1]);
		
		HashMap<String, Integer>time_map = new HashMap<String, Integer>();
		time_map.put("HOUR", hh);
		time_map.put("MINUTE", mm);
		
		return time_map;
	}
	
	/**
	 * TimePickerに現在の時刻を設定
	 * 
	 * @param context コンテキスト
	 * @param eventListener 選択時に実行されるイベントリスナー
	 * @param init true:初期表示<br>false:現在時刻表示
	 * 
	 * @return TimePickerオブジェクト
	 */
	public static TimePickerDialog setTimePicker(Context context, TimePickerDialog.OnTimeSetListener eventListener, boolean init) {
		// 現在時刻の取得
		String nowTime = CommonUtil.getTime();
		
		// 分割
		@SuppressWarnings("unchecked")
		HashMap<String, Integer>split_time = CommonUtil.splitTime(nowTime);
		int hour = split_time.get("HOUR");
		int minute = split_time.get("MINUTE");
		
		TimePickerDialog tp;
		if(init) {
			tp = new TimePickerDialog(context, eventListener, 0, 0, true);
		} else {
			tp = new TimePickerDialog(context, eventListener, hour, minute, true);
		}
		return tp;
	}
	
	/**
	 * TimePickerに任意の時間を設定
	 * 
	 * @param context コンテキスト
	 * @param eventListener 選択時に実行されるイベントリスナー
	 * @param time 設定時間
	 * @return TimePickerオブジェクト
	 */
	public static TimePickerDialog setTimePicker(Context context, TimePickerDialog.OnTimeSetListener eventListener, String time) {
		// 分割
		@SuppressWarnings("unchecked")
		HashMap<String, Integer>split_time = CommonUtil.splitTime(time);
		int hour = split_time.get("HOUR");
		int minute = split_time.get("MINUTE");
		
		return new TimePickerDialog(context, eventListener, hour, minute, true);
	}
}
