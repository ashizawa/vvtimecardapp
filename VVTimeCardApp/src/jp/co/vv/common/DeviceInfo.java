/*
 * DeviceInfo.java
 * 
 * Create on H,Ashizawa 2014.07.03  新規作成
 */
package jp.co.vv.common;

import java.util.HashMap;

import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Display;
import android.view.WindowManager;

/**
 * 各種デバイス情報取得処理
 * 
 * @author H.Ashizawa
 */
public class DeviceInfo {
	
	//　定数：通信状態
	/** 通信圏外 */
	public static final int NETWORK_NONE = 0;
	/** モバイル */
	public static final int NETWORK_MOBILE = 1;
	/** WiFi */
	public static final int NETWORK_WIFI = 2;
	
	
	/**
	 * 端末の画面サイズを取得
	 * 
	 * @param conext
	 * @return　画面サイズ 
	 */
	public static HashMap getDisplaySize(Context conext) {
		// ウィンドウマネージャのインスタンス取得
		WindowManager windowManeger = (WindowManager)conext.getSystemService(Context.WINDOW_SERVICE);
		// ディスプレイのインスタンス生成
		Display disp = windowManeger.getDefaultDisplay();

		float width;
		float height;
		
		// AndroidのAPIレベルによって画面サイズ取得方法が異なるので条件分岐
        if (Integer.valueOf(android.os.Build.VERSION.SDK_INT) < 13) {
            width = disp.getWidth();
            height = disp.getHeight();
        } else {
            Point size = new Point();
            disp.getSize(size);
            width = size.x;
            height = size.y;
        }
        
        HashMap<String, Float> sizeData = new HashMap<String, Float>();
        sizeData.put("width", width);
        sizeData.put("height", height);
        
        return sizeData;
	}
	
	/** 
	 * デバイスのネットワーク接続状態を取得
	 * 
	 * @param conext コンテキスト
	 * @return ネットワーク状態<br>NETWORK_NONE(=0):圏外, NETWORK_MOBILE(=1):モバイル, NETWORK_WIFI(=2):WiFi
	 */
	public static int getNetworkStatus(Context conext) {
		ConnectivityManager connectManeger = (ConnectivityManager) conext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = connectManeger.getActiveNetworkInfo();
		
		if(netInfo == null) {
			// ネットワーク接続：不可
			return NETWORK_NONE;
		}
		if (netInfo.isConnected()) {
            // ネットワーク接続：可
            if (netInfo.getTypeName().equals("WIFI")) {
                return NETWORK_WIFI;
            } else if (netInfo.getTypeName().equals("mobile")) {
            	return NETWORK_MOBILE;
            }
        } else {
        	// ネットワーク接続：不可
        	return NETWORK_NONE;
        }
		return NETWORK_NONE;
	}
	
}
