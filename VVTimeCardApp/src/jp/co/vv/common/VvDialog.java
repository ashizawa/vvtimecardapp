package jp.co.vv.common;

import android.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

/**
 * アラートダイアログクラス
 * 
 * @author VV77
 */
public class VvDialog {
	
	// 各種アイコン定義
	/** INFOアイコン(i) */
	public static final int ICON_INFO = R.drawable.ic_dialog_info;
	/** WARNINGアイコン(!) */
	public static final int ICON_WARNING = R.drawable.ic_dialog_alert;
	/** ERRORアイコン(X) */
	public static final int ICON_ERROR = R.drawable.ic_delete;
	
	/**
	 * ダイアログ表示（リソースID指定）
	 * 
	 * @param context
	 * @param icon
	 * @param titleId
	 * @param msgId
	 * @param positiveBtnId
	 * @param negativeBtnId
	 * @param positive
	 * @param negative
	 * @param cancelable
	 */
	public static void show(
				Context context,
				int icon,
				int titleId,
				int msgId,
				int positiveBtnId,
				int negativeBtnId,
				OnClickListener positive,
				OnClickListener negative,
				Boolean cancelable) {
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		
        // アラートダイアログのタイトルを設定します
		if(titleId != 0) {
			alertDialogBuilder.setTitle(titleId);
		}
		
        // アラートダイアログのメッセージを設定します
        alertDialogBuilder.setMessage(msgId);
        
        // アイコン設定
        if(icon != 0) {
        	alertDialogBuilder.setIcon(icon);
        }
        
        // アラートダイアログの肯定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
        if(positive != null) {
        	alertDialogBuilder.setPositiveButton(positiveBtnId,positive);
        }
        
        // アラートダイアログの否定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
        if(negative != null) {
        	alertDialogBuilder.setNegativeButton(negativeBtnId,negative);
        }
        
        // アラートダイアログのキャンセルが可能かどうかを設定します
        alertDialogBuilder.setCancelable(cancelable);
        
        // アラートダイアログを表示します
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
	}
	
	/**
	 * 「閉じる」ボタンが一つあるダイアログ
	 * 
	 * @param context
	 * @param icon
	 * @param titleId
	 * @param msgId
	 * @param cancelable
	 */
	public static void show(
			Context context,
			int icon,
			int titleId,
			int msgId,
			int positiveId,
			Boolean cancelable) {
	
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		
	    // アラートダイアログのタイトルを設定します
		if(titleId != 0) {
			alertDialogBuilder.setTitle(titleId);
		}
		
	    // アラートダイアログのメッセージを設定します
	    alertDialogBuilder.setMessage(msgId);
	    
	    // アイコン設定
	    if(icon != 0) {
	    	alertDialogBuilder.setIcon(icon);
	    }
	    
	    // アラートダイアログの肯定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
		alertDialogBuilder.setPositiveButton(positiveId, 
				new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	        	dialog.dismiss();
	        }
	    });
	    
	    // アラートダイアログのキャンセルが可能かどうかを設定します
	    alertDialogBuilder.setCancelable(cancelable);
	    
	    // アラートダイアログを表示します
	    AlertDialog alertDialog = alertDialogBuilder.create();
	    alertDialog.show();
	}
}
